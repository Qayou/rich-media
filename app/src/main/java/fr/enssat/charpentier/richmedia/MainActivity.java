package fr.enssat.charpentier.richmedia;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    static private String MAPVIEW_BUNDLE_KEY = "MAP_VIEW_KEY";
    private VideoView videoView;
    private WebView webView;
    private MapView mapView;
    private MediaController mediaController;
    private JsonParser parser;
    private JSONArray waypoints;
    private JSONArray keywordsArray;
    private Handler handler;
    private LinearLayout llkeywords;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        this.parser = new JsonParser(this);
        this.handler = new Handler();
        this.llkeywords = findViewById(R.id.llkeywords);
        this.keywordsArray = this.parser.getKeywords();

        // MAP
        Bundle mapViewBundle = null;
        if (savedInstanceState != null) {
            mapViewBundle = savedInstanceState.getBundle(MAPVIEW_BUNDLE_KEY);
        }
        this.mapView = findViewById(R.id.mapView);
        this.mapView.onCreate(mapViewBundle);
        this.waypoints = this.parser.getWayPoints();

        // VIDEO
        this.videoView = findViewById(R.id.videoView);
        this.mediaController = new MediaController(MainActivity.this);
        this.mediaController.setAnchorView(this.videoView);

        this.videoView.setMediaController(mediaController);
        this.videoView.setVideoURI(Uri.parse(this.parser.getVideoUrl()));

        this.videoView.requestFocus();
        this.videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                videoView.start();
                mediaController.setAnchorView(videoView);
                videoView.postDelayed(mShowProgress, 1000);
            }
        });

        // WEBVIEW
        this.webView = findViewById(R.id.webView);
        WebViewClient webViewClient = new WebViewClient();
        this.webView.setWebViewClient(webViewClient);
        this.webView.loadUrl(this.parser.getWebUrl());

        // CHAPTERS
        ArrayList<Chapter> chapters = this.parser.getChapters();

        LinearLayout ll = findViewById(R.id.ll);

        for(final Chapter chapter : chapters) {
            Log.d("Chapters", chapter.getLabel());
            Button button = new Button(this);
            button.setText(chapter.getLabel());

            button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    videoView.seekTo(chapter.getTimestamp() * 1000);
                }
            });

            ll.addView(button);
        }

        this.initMap();
    }

    private int setProgress() {
        Log.i("INFO", "setProgress");
        int position = this.videoView.getCurrentPosition();

        ArrayList<String> keywords = new ArrayList<>();

        for (int i = 0; i < keywordsArray.length(); i++) {
            try {
                JSONObject keyword = keywordsArray.getJSONObject(i);
                if (keyword.getInt("pos") * 1000 < position) {
                    keywords.clear();
                    JSONArray keywordsArray = keyword.getJSONArray("data");

                    for (int k = 0; k < keywordsArray.length(); k++) {
                        keywords.add(keywordsArray.getJSONObject(k).getString("title"));
                    }
                } else {
                    break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        this.llkeywords.removeAllViews();

        for (String s : keywords) {
            Log.i("KEYWORD", s);
            TextView textView = new TextView(this);
            textView.setText(s);
            textView.setPadding(0, 0, 50, 0);
            this.llkeywords.addView(textView);
        }

        return position;
    }

    private final Runnable mShowProgress = new Runnable() {
        @Override
        public void run() {
            if (videoView.isPlaying()) {
                setProgress();
                handler.removeCallbacks(mShowProgress);
                handler.postDelayed(mShowProgress, 1000);
            }
        }
    };

    private void initMap(){
        this.mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                try {
                    long lat = 0;
                    long lng = 0;
                    String label = "";
                    int timestamp = 0;
                    for (int i = 0; i < waypoints.length(); i++) {
                        lat = waypoints.getJSONObject(i).getLong("lat");
                        lng = waypoints.getJSONObject(i).getLong("lng");
                        label = waypoints.getJSONObject(i).getString("label");
                        timestamp = waypoints.getJSONObject(i).getInt("timestamp");
                        Marker marker = googleMap.addMarker(new MarkerOptions()
                                .position(new LatLng(lat,lng))
                                .title(label));
                        marker.setTag(timestamp);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                    @Override
                    public boolean onMarkerClick(Marker marker) {
                        int timestamp = (int)marker.getTag();
                        videoView.seekTo(timestamp * 1000);
                        return false;
                    }
                });
            }
        });
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Bundle mapViewBundle = outState.getBundle(MAPVIEW_BUNDLE_KEY);
        if (mapViewBundle != null) {
            mapViewBundle = new Bundle();
            outState.putBundle(MAPVIEW_BUNDLE_KEY, mapViewBundle);
        }

        this.mapView.onSaveInstanceState(mapViewBundle);
    }

    @Override
    protected void onResume() {
        super.onResume();
        this.mapView.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        this.mapView.onResume();
    }

    @Override
    protected void onStart() {
        super.onStart();
        this.mapView.onStart();
    }

    @Override
    protected void onStop() {
        super.onStop();
        this.mapView.onStop();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        this.mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        this.mapView.onLowMemory();
    }
}
