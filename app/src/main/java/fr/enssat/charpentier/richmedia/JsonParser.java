package fr.enssat.charpentier.richmedia;

import android.content.Context;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;

/**
 * Created by caillou on 1/11/18.
 */

public class JsonParser {
    private Context context;
    private JSONObject metadata;

    public JsonParser(Context _context) {
        this.context = _context;

        try {
            // Read the JSON files
            InputStream is = context.getResources().openRawResource(R.raw.chapters);
            Writer writer = new StringWriter();
            char[] buffer = new char[1024];
            try {
                Reader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                int n;
                while ((n = reader.read(buffer)) != -1) {
                    writer.write(buffer, 0, n);
                }
                is.close();
            } catch (Exception e) {
                e.printStackTrace();
            }

            // Create a JSONObject from the JSON file
            String metadataJSON = writer.toString();
            this.metadata = new JSONObject(metadataJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Chapter> getChapters() {
        ArrayList<Chapter> chapters = new ArrayList<>();

        // Extract the video array
        try {
            JSONArray videos = this.metadata.getJSONArray("Chapters");

            // Extract each video and create VideoMetadata objects
            for (int i = 0; i < videos.length(); i++) {
                JSONObject videoMetadataJSON = (JSONObject) videos.get(i);

                Chapter chapter = new Chapter(videoMetadataJSON.getString("title"), videoMetadataJSON.getInt("pos"));

                chapters.add(chapter);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return chapters;
    }

    public JSONArray getWayPoints() {
        JSONArray waypoints = null;

        try {
            waypoints = this.metadata.getJSONArray("Waypoints");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return waypoints;
    }

    public JSONArray getKeywords() {
        JSONArray keywords = null;

        try {
            keywords = this.metadata.getJSONArray("Keywords");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return keywords;
    }

    public String getWebUrl() {
        String url = null;

        try {
            url = this.metadata.getJSONObject("Film").getString("synopsis_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return url;
    }

    public String getVideoUrl() {
        String fileUrl = null;

        try {
            fileUrl = this.metadata.getJSONObject("Film").getString("file_url");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return fileUrl;
    }
}
