package fr.enssat.charpentier.richmedia;

/**
 * Created by caillou on 1/11/18.
 */

class Chapter {
    private String label;
    private int timestamp;

    public Chapter(String _label, int _timestamp) {
        this.label = _label;
        this.timestamp = _timestamp;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

    public int getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(int timestamp) {
        this.timestamp = timestamp;
    }
}
